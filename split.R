cat('\n+...Creación de splits...')
### ------- Creación de un split por cada año ------- ###
if(test_predict %in% c('2016','2017')){
  aniosTRAIN <- train[,unique(ANIO)]
  numFolds <- length(aniosTRAIN)-2
  foldsTrain <- list()
  foldsVal <- list()
  train[,ID := .I]
  for (j in 1:numFolds){
    i <- j + 2
    foldsVal[[j]] <- train[ANIO == aniosTRAIN[i],ID]
    foldsTrain[[j]] <- train[ANIO < aniosTRAIN[i], ID]
  }  
  train[,ID := NULL]
}else if(test_predict == 'union'){
  aniosTRAIN <- train[,unique(ANIO)]
  numFolds <- length(aniosTRAIN)-3
  foldsTrain <- list()
  foldsVal <- list()
  train[,ID := .I]
  for (j in 1:numFolds){
    i <- j + 2
    foldsVal[[j]] <- train[ANIO %in% c(aniosTRAIN[i],aniosTRAIN[i+1]),ID]
    foldsTrain[[j]] <- train[ANIO < aniosTRAIN[i], ID]
  }  
  train[,ID := NULL]
}

