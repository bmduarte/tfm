cat('\n+...Importación de datos...')
if(test_predict == '2016'){
train <- fread(paste0(input_path,'train.csv'), na.strings = c('NA','N/A','null','NULL','','NaN'), encoding ='UTF-8')
test <- fread(paste0(input_path,'test_2016.csv'), na.strings = c('NA','N/A','null','NULL','','NaN'), encoding = 'UTF-8')
test[,CANTIDADSALIDA := NA]
data <- rbindlist(list(train,test))
}else if(test_predict == '2017'){
  train <- fread(paste0(input_path,'train.csv'), encoding ='UTF-8',na.strings = c('NA','N/A','null','NULL','','NaN'))
  train_aux <- fread(paste0(input_path,'test_2016.csv'), encoding ='UTF-8',na.strings = c('NA','N/A','null','NULL','','NaN'))
  train[,cambiar := 'train']
  train_aux[,cambiar := 'aux']
  train_aux[,CANTIDADSALIDA := NA]
  setcolorder(train_aux,names(train))
  predictions <- list.files(paste0(output_path,'preds'), full.names = T)
  target_xgb <- fread(predictions[grepl('xgb',predictions)])[,pred]
  target_lgb <- fread(predictions[grepl('lgb',predictions)])[,pred]
  dt_target <- data.table(target_xgb = target_xgb, target_lgb = target_lgb)
  dt_target[,target_mean := (target_xgb+target_lgb)/2]
  target_mean <- dt_target[,target_mean]
  
  test <- fread(paste0(input_path,'test_2017.csv'), encoding ='UTF-8',na.strings = c('NA','N/A','null','NULL','','NaN'))
  test[,cambiar := 'test']
  test[,CANTIDADSALIDA := NA]
  setcolorder(test,names(train))
  data <- rbindlist(list(train,train_aux,test))
  
}else if(test_predict == 'union'){
  train <- fread(paste0(input_path,'train.csv'), encoding ='UTF-8',na.strings = c('NA','N/A','null','NULL','','NaN'))
  test_2016 <- fread(paste0(input_path,'test_2016.csv'), na.strings = c('NA','N/A','null','NULL','','NaN'), encoding = 'UTF-8')
  test_2017 <- fread(paste0(input_path,'test_2017.csv'), na.strings = c('NA','N/A','null','NULL','','NaN'), encoding = 'UTF-8')
  test <- rbindlist(list(test_2016,test_2017))
  test[,CANTIDADSALIDA := NA]
  data <- rbindlist(list(train,test))
}
