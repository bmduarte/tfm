
procesado <- function(modelo){
  cat('\n+...Procesado final de predicciones para modelo ',modelo,'...')
  files <- list.files(paste0(output_path,'preds/'),modelo, full.names = T)
  preds <- data.table()
  for(file in files){
    preds <- rbindlist(list(preds,fread(file, encoding = 'UTF-8')))
    
  }
  setnames(preds,'pred','CANTIDADSALIDA_PRED')
  preds[,`:=`(ANIO = gsub('.*_','',Id) , MES = gsub('.*_','',substr(Id,1,nchar(Id) - 5)),
              ID = as.integer(gsub('_.*','',Id)))]
  predictions <- data_ids[preds, nomatch = 0, allow.cartesian = T, on = c(ID_ARTICULO = 'ID')]
  predictions <- unique(predictions[,.(ARTICULO,CENTROCONSUMO,ID_ORGANOGESTOR,DESC_PROVINCIA,ANIO,MES,CANTIDADSALIDA_PRED)])
  
  fwrite(predictions,paste0(output_path,'final_results_',modelo,'_impute_',gsub(':','.',Sys.time()),'.csv'))
  
}

list_preds <- list.files(paste0(output_path,'preds/'))
preds_2016 <- list_preds[grepl('2016',list_preds)]
preds_2017 <- list_preds[grepl('2017',list_preds)]

if (test_predict != 'union'){
  if(length(preds_2016) == 2 && length(preds_2017) == 2){
    cat('\n+...Procesado final de predicciones...')
    data_ids <- data.table()
    files_ids <- list.files(input_path,'id_data',full.names = T)
    for (file in files_ids){
      data_ids <- rbindlist(list(data_ids,fread(file,encoding = 'UTF-8')))
    }
    for (modelo in c('xgb','lgb')){
      procesado(modelo)
    }
  }
}else{
  for (modelo in c('xgb','lgb')){
    procesado(modelo)
  }
}





