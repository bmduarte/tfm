## -------------------------- Main -------------------------- ##
### ----------- Librerías ----------- ###
cat('\n...Librerías necesarias...')
library(readxl)
library(data.table)
library(caret)
library(DMwR)
library(xgboost)
library(lightgbm)
library(Boruta)
### --------- Configuración --------- ###
source('scripts/configuracion.R', encoding = 'UTF-8')

### ------ Pasar de extensión .xlsx a .csv ------ ###
if(xlsx_to_csv){
  to_csv_data()
}
### -------- Funciones -------- ###
source('scripts/funciones.R',encoding = 'UTF-8')

### ----------- Import ----------- ###
source('scripts/import.R', encoding = 'UTF-8')

### ---------- Ingeniería de variables ---------- ###

## --- Pasamos datos diarios a mensuales --- ##
data <- monthly_data(data) 
## --- Añadimos predicciones del año 2016 al train (para modelo que predice el año 2017) --- ##
if (test_predict == '2017'){
  data[,CANTIDADSALIDA_MES := ifelse(cambiar == 'aux', target_mean,CANTIDADSALIDA_MES)]
  data[,cambiar := NULL]
}
## --- Creación, transformación y eliminación de variables --- ##
data <- feature_engineering(data)
data <- add_variables(data,2)
data <- constant_or_NA_var(data)
data <- count_NA_0(data)
data <- change_num_to_char(data)
data <- ordinales(data)


### -------- Clase de cada variable -------- ###
data.x.class <- data.table(feature = names(data), class = data[,.(cl = paste(lapply(.SD,class)),collapse = '')]$cl)
data.x.class <- data.x.class[!feature %in% c('ID','ID_ARTICULO','CANTIDADSALIDA_MES')]
fwrite(data.x.class,paste0(input_path,'var_class/data_',test_predict,'.csv'))

### -------- Normalización de datos -------- ###
num_var <- data.x.class[class != 'character',feature]
scale <- mean_sd_variables(data,num_var)
data <- normalize_var(data,scale)
fwrite(data,paste0(input_path,'data_process/data_',test_predict,'_impute_',impute,'.csv'))

### -------- Imputación de valores -------- ###
if(impute){
  data <- knn_impute(data,data.x.class)
  fwrite(data,paste0(input_path,'data_process/data_knn_',test_predict,'_impute_',impute,'.csv'))
}


### ------ Label Encoding Frequency ----- ###
count_frecuency_data(data,0,data.x.class)
data <- label_encoding(data)
fwrite(data,paste0(input_path,'data_process/data_LE_',test_predict,'_impute_',impute,'.csv'))

### ----- Definición de train y test ----- ###
train <- data[!is.na(CANTIDADSALIDA_MES)]
test <- data[is.na(CANTIDADSALIDA_MES)]

### ---  Método Boruta --- ###
if(features_selection){
  boruta.trn(train)
}

train <- train[order(ID_ARTICULO,ANIO,MES)]
train_ids <- train[,ID]
target <- train[,CANTIDADSALIDA_MES]
train[,`:=`(ID = NULL, CANTIDADSALIDA_MES = NULL)]
feature_names <- names(train)
test <- test[order(ID_ARTICULO,ANIO,MES)]
test_ids <- test[,ID]
test[,`:=`(ID = NULL, CANTIDADSALIDA_MES = NULL)]
setcolorder(test,feature_names)
cat(paste0('\n+...Las columnas del train y test son iguales y en el mismo orden: ', all.equal(names(train),names(test))))

### ----- Creación de splits ----- ###
source('scripts/split.R', encoding = 'UTF-8')

### ----- Modelado ----- ###
for (modelo in c('xgb','lgb')){
  source('scripts/modelado.R', encoding = 'UTF-8')
}

### --- Procesado de predicciones --- ###
source('scripts/procesado_final.R', encoding = 'UTF-8')
